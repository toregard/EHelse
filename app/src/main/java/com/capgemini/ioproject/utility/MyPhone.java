package com.capgemini.ioproject.utility;

import android.app.Activity;
import android.provider.Settings;

/**
 * Created by toanders on 16.02.2017.
 * Set <uses-permission android:name="android.permission.READ_PHONE_STATE" />
 */
public class MyPhone {

    public static String getMacAddress(Activity activity){
        String deviceId = Settings.Secure.getString(activity.getBaseContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        return deviceId;
    }
}
