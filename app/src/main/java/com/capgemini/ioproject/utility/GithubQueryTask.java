package com.capgemini.ioproject.utility;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.URL;

/**
 * Created by toanders on 16.02.2017.
 */

public class GithubQueryTask extends AsyncTask<URL, Void, String> {

    // COMPLETED (2) Override the doInBackground method to perform the query. Return the results. (Hint: You've already written the code to perform the query)
    @Override
    protected String doInBackground(URL... params) {
        URL searchUrl = params[0];
        String service = null;
        try {
            service = NetworkUtils.getResponseFromHttpUrl(searchUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return service;
    }

    // COMPLETED (3) Override onPostExecute to display the results in the TextView
    @Override
    protected void onPostExecute(String results) {
        if (results != null && !results.equals("")) {
            Log.d("Info  Knapp resultat",results.toString());
        }
    }
}
