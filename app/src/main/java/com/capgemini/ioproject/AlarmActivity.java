package com.capgemini.ioproject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.capgemini.ioproject.utility.GithubQueryTask;
import com.capgemini.ioproject.utility.MyPhone;
import com.capgemini.ioproject.utility.NetworkUtils;

import java.io.IOException;
import java.net.URL;

public class AlarmActivity extends AppCompatActivity {
    Button rodButton;
    Button blaaButtion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);

        rodButton = (Button) findViewById(R.id.rodButton);
        blaaButtion = (Button) findViewById(R.id.blaaButton);
    }

    public void helpButtonAction(View view){
        String url = getString(R.string.pivotalservice);
        Log.d("Info","helpButtonAction start kall webservice");
        URL pivotalservice = NetworkUtils.pivotalserviceUrl(url, MyPhone.getMacAddress(this),"helpButton");
        new GithubQueryTask().execute(pivotalservice);
    }

    public void annetButtonAction(View view){
        Log.d("Info","gotoMainButtonAction");
        Context context = AlarmActivity.this;
        Class infoValgActivity = InfoActivity.class;
        Intent startInfoValgActivity = new Intent(context, infoValgActivity);
        //startFullscreenActivity.putExtra(Intent.EXTRA_TEXT, "Test");
        startActivity(startInfoValgActivity);
         }

    // COMPLETED (1) Create a class called GithubQueryTask that extends AsyncTask<URL, Void, String>


}
