package com.capgemini.ioproject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setTitle("GPS");
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.miCompose:
                composeMessage();
                return true;
            case R.id.miProfile:
                showProfileView();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void composeMessage(){
        Log.d("Info","composeMessage");
    }

    private void showProfileView(){
        Log.d("Info","showProfileView");
    }

    public void gotoAlarmButtonAction(View view){
        Log.d("Info","gotoAlarmButtonAction");
        Context context = MainActivity.this;
        Class infoValgActivity = AlarmActivity.class;
        Intent startInfoValgActivity = new Intent(context, infoValgActivity);
        //startFullscreenActivity.putExtra(Intent.EXTRA_TEXT, "Test");
        startActivity(startInfoValgActivity);
    }
}
