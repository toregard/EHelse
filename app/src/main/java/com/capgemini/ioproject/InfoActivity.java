package com.capgemini.ioproject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.capgemini.ioproject.utility.GithubQueryTask;
import com.capgemini.ioproject.utility.MyPhone;
import com.capgemini.ioproject.utility.NetworkUtils;

import java.net.URL;

public class InfoActivity extends AppCompatActivity {
    Button ringMegButton;
    Button gaarTurButton;
    static final String GAATUR="Går tur";
    static final String SLUTTTUR="Slutt tur";
    static final String GAARTURSTATE="GAARTURSTATE";
    public static final String PREFS_NAME = "MyPrefsFile";
    private boolean gaarState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        ringMegButton = (Button) findViewById(R.id.blaaCallButton);
        gaarTurButton = (Button) findViewById(R.id.WalkButton);

        // Restore preferences
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        gaarState = settings.getBoolean(GAARTURSTATE, false);

        setTurButtonTextState(getGaarTurState());
    }

    private void setTurButtonTextState(boolean state){
        if(!state){
            Log.d("Info","Create Starter å gå tur sett true ");
            gaarTurButton.setText(SLUTTTUR);

        }else{ //SLUTTTUR
            Log.d("Info","Create Slutter å gå tur sett false");
            gaarTurButton.setText(GAATUR);
        }
    }

    private boolean getGaarTurState(){
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        boolean gaarState = settings.getBoolean(GAARTURSTATE, false);
        return gaarState;
    }

    private  void setGaarTurState(Boolean state){
        Log.d("setGaarTurState",state.toString());
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(GAARTURSTATE, state);
        this.gaarState=state;
        editor.commit();
    }



    public void ringMegButtonAction(View view){
        Log.d("Info","ringMegButtonAction");

        String url = getString(R.string.pivotalservice);
        Log.d("Info","ringMegButtonAction start kall webservice");
        URL pivotalservice = NetworkUtils.pivotalserviceUrl(url, MyPhone.getMacAddress(this),"ringMegKnapp");
        new GithubQueryTask().execute(pivotalservice);

        Context context = InfoActivity.this;
        Class alarmActivity = AlarmActivity.class;
        Intent startAlarmActivityActivity = new Intent(context, alarmActivity);
        startActivity(startAlarmActivityActivity);
    }

    public void gaarTurButtonAction(View view){
        String url = getString(R.string.pivotalservice);

        if(!getGaarTurState()){
            Log.d("Info","Starter å gå tur");
            setGaarTurState(true);
        }else{ //SLUTTTUR
            Log.d("Info","Slutter å gå tur");
            setGaarTurState(false);
        }
        setTurButtonTextState(this.gaarState);
        //Går tur, gå tilbake til hovedmeny
        if(getGaarTurState()){
            Context context = InfoActivity.this;
            Class alarmActivity = AlarmActivity.class;
            Intent startAlarmActivityActivity = new Intent(context, alarmActivity);
            startActivity(startAlarmActivityActivity);
        }

        URL pivotalservice;
        StringBuffer s = new StringBuffer(1);
        if(getGaarTurState()){
            s.append("StoppetTurKnapp");
        }else
        {
            s.append("StartetTurKnapp");
        }
        pivotalservice = NetworkUtils.pivotalserviceUrl(url, MyPhone.getMacAddress(this),s.toString());
        new GithubQueryTask().execute(pivotalservice);

    }
}
